# 3D printing utility files and build system

This is a set of files I use for making 3D objects.

It mostly consists of my own work, but occasionally I include other
people's work if that other work has a suitable licence.

# How to use

To add to your project:

```
git subtree add -P diziet-utils https://salsa.debian.org/iwj/3d-utils.git main
```

To update to a new version:

```
git subtree pull -P diziet-utils https://salsa.debian.org/iwj/3d-utils.git main
```

# Licence

Everything is GPLv3+ (or compatible).
